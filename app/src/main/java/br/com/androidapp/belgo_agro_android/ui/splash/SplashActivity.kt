package br.com.androidapp.belgo_agro_android.ui.splash

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import br.com.androidapp.belgo_agro_android.R

class SplashActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setTheme(R.style.AppTheme_SplashScreen)
    }

}